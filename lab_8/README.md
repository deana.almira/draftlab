Mandatory
1.  Membuat halaman untuk Login menggunakan OAuth
    1.  Mendaftarkan aplikasi ke facebook developer page
        1.  Buka https://developers.facebook.com/
        2.  Daftar Sebagai Pengembang Facebook
        3.  Buat Aplikasi baru dengan mengklik tombol
        Add a New App dapat ditemukan di kanan atas
        4.  Masuk ke halaman aplikasi yang sudah dibuat dan pilih Add Product, kemudian tambahkan Facebook Login
        5.  Pada bagian setting silakan tambahkan platform dan pilih website, lalu sesuaikan isi dari site url. 
        6.  Cara lain: Anda mungkin juga mendapati halaman Quickstart, pilih Web sebagai platform.
        7.  Applikasi telah berhasil didaftarkan.

    2.  Melakukan OAuth Login menggunakan Facebook
        1. Membuat file lab8.js isi dengan fungsi untuk:
        menginisiasi js secara asynchronuos, facebooklogin, 
        2. Menambahkan lab8.js ke base.html:
        <script src="{% static 'js/lab8.js' %}"></script>
        3. Membuat buton login di lab_8.html
        <button onclick="facebookLogin()">Login with Facebook</button>
        
    3.  Menampilkan informasi dari user yang login menggunakan API Facebook.
        Membuat fungsi getUserData() di lab8.js yg mengembalikan id dan name
    
    4.  Melakukan post status facebook
        1.  Menambah permission di fungsi facebooklogin di lab8.js
        2.  Membuat fungsi postFeed di lab8.js
        
    5.  Menampilkan post status pada halaman lab_8.html
    
    6.  Melakukan Logout
        Menambah method facebookLogout
        
    7.  Implementasi css yang indah dan responsive

2.  Jawablah pertanyaan yang ada di dokumen ini dengan menuliskannya pada buku catatan atau pada source code kalian yang dapat ditunjukkan saat demo
    1.  Dengan menambahkan permission user_posts dan publish_actions, apa saja yang dapat dilakukan oleh aplikasi kita menggunakan Graph API?
        
        user_posts: Provides access to the posts on a person's Timeline. Includes their own posts, posts they are tagged in, and posts other people make on their Timeline.

        publish_actions: Provides access to publish Posts, Open Graph actions, and other activity on behalf of a person using your app.
    
    2.  Apakah yang dimaksud dengan fungsi callback?
        fungsi yang bisa digunakan berulang2 dan bisa dipakai oleh fungsi lain

        digunakan untuk:
        1. Asynchronous execution (Node.js, ajax)
        2. Event Listeners/Handlers (jquery dll)
        3. setTimeout and setInterval methods
        4. Generalization : Kode jadi lebih ringkas, lebih indah,dan mudah dibaca/maintenance