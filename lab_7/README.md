1.  Header: 
    <li><a href="{% url 'lab-7:friend-list' %}"> Friend List </a></li>

2.  Terdapat list yang berisi daftar mahasiswa fasilkom, yang dipanggil dari django model.
    views.py mendapatkan list mahasiswa dari csui_helper:
        from .models import Friend
        mahasiswa_list = csui_helper.instance.get_mahasiswa_list()
        friend_list = Friend.objects.all()

    lab_7.html untuk menampilkan list mahasiswa dengan for loop:
        {% if mahasiswa_list %}
            {% for mahasiswa in mahasiswa_list %}
                <a class="list-group-item clearfix">
                    {{ mahasiswa.nama }} ({{ mahasiswa.npm }})

3.  Buatlah tombol untuk dapat menambahkan list mahasiswa kedalam daftar teman (implementasikan menggunakan ajax).
    lab_7.html, membuat button ketika diklik akan memanggil method addFriend:
        <span class="btn btn-xs btn-default" onClick="addFriend('{{ mahasiswa.nama }}', '{{ mahasiswa.npm }}')">
            Tambah sebagai teman
        </span>

    javascript di lab_7.html, membuat ajax jika sukses maka akan add friend, jika error akan menampilkan pesan error:

    method POST untuk membuat data baru di API:
        var addFriend = function(nama, npm) {
        $.ajax({
            ...
        });
    };

    method GET untuk mengambil data dari API: 
    $("#add-friend").on("submit", function (event) {
       var name = $("#field_name").val();
       var npm = $("#field_npm").val();
       var corsProxy = 'https://cors-anywhere.herokuapp.com/';
       var obj = new Object(); obj.access_token = token; obj.client_id = cid;
       var param = $.param(obj);
       $.ajax({
            method: "GET",
            url: corsProxy+'https://api-dev.cs.ui.ac.id/siakngcs/mahasiswa/'+npm+'/?'+param,
            success: function(data) {
                addFriend(data.nama, data.npm)
            },
            error: function(error) {
                console.log(error)
                alert("\n\t\t\tNPM tidak valid:\n\n'Tidak ada mahasiswa dengan NPM tersebut'")
            }
        });
        event.preventDefault();
    });

    di views.py, membuat fungsi add dan menambahkan ke daftar teman
    def add_friend(request):
        if request.method == 'POST':
            name = request.POST['name']
            npm = request.POST['npm']
            connect = Friend.objects.filter(npm = npm).exists()
            if (not connect):
                friend = Friend(friend_name=name, npm=npm)
                friend.save()
            data = model_to_dict(friend)
            return HttpResponse(data)
    
    di views.py, membuat fungsi model_to_dict
    def model_to_dict(obj):
        data = serializers.serialize('json', [obj,])
        struct = json.loads(data)
        data = json.dumps(struct[0]["fields"])
        return data

4.  Mengimplentasikan validate_npm untuk mengecek apakah teman yang ingin dimasukkan sudah ada didalam daftar teman atau belum.
    Membuat konfigurasi URL di lab_7/urls.py
    Menambah method validate_npm di views.py
    Menambah javascript untuk validate_npm:
        $("#field_npm").change(function () {
            ...
        }
    javascript untuk: jika memasukan input di kolom NPM dengan id="field_npm" (ingin membuat data baru di API)ketika mengklik "tambah" maka npm akan diproses, mengecek npm tersebut, jika sudah ada akan menampilkan pesan (pop up box)
    
5.  Membuat pagination (hint: salah satu data yang didapat dari kembalian api.cs.ui.ac.id adalah next dan previous yang bisa digunakan dalam membuat pagination)
    Membuat div pagination di lab_7.html:
    Untuk setiap kasus: jika halaman sudah punya halaman sebelum dan sesudah, current.
    Menampilkan page x of y, previous, dan next
    membaca var token dan client id untuk authorization
        <div class="pagination">
            <span class="step-links">
                {% if mahasiswa_list.has_previous %}
                    <a href="?page={{ mahasiswa_list.previous_page_number }}">previous</a>
                {% endif %}
                <span class="current">
                    Page {{ mahasiswa_list.number }} of {{ mahasiswa_list.paginator.num_pages }}.
                </span>
                {% if mahasiswa_list.has_next %}
                    <a href="?page={{ mahasiswa_list.next_page_number }}">next</a>
                {% endif %}
            </span>
            {% if auth %}
                <div id="token" hidden="true">{{ auth.access_token }}</div>
                <div id="cid" hidden="true">{{ auth.client_id }}</div>
            {% endif %}
        </div>`

    Kemudian tambah var token dan client id di block javascript
    var token = document.getElementById("token").innerHTML;
    var cid = document.getElementById("cid").innerHTML;

    Membuat method di views.py:
    1.  Import Paginator:
        from django.core.paginator import Paginator,EmptyPage,PageNotAnInteger

    2.  Membuat method untuk mendevinisikan variabel2 di method index untuk memanggil methode paginate_data
        def index(request):
            ...
            #Paginator
            page = request.GET.get('page',1)
            paginate_data = paginate_page(page, mahasiswa_list)
            mahasiswa = paginate_data['data']
            page_range = paginate_data['page_range']
            response = {"mahasiswa_list": mahasiswa, "friend_list": friend_list,"page_range": page_range, "auth": auth}

    3.  Membuat method paginate_data untuk mendapatkan index dari current page dan memperbaruinya
        def paginate_page(page, data_list):
            paginator = Paginator(data_list, 25)
            try:
                data = paginator.page(page)
            except PageNotAnInteger:
                data = paginator.page(1)
            except EmptyPage:
                data = paginator.page(paginator.num_pages)

            # Get the index of the current page
            index = data.number - 1
            # This value is maximum index of your pages, so the last page - 1
            max_index = len(paginator.page_range)
            # You want a range of 10, so lets calculate where to slice the list
            start_index = index if index >= 25 else 0
            end_index = 25 if index < max_index - 25 else max_index
            # Get our new page range. In the latest versions of Django page_range returns 
            # an iterator. Thus pass it to list, to make our slice possible again.
            page_range = list(paginator.page_range)[start_index:end_index]
            paginate_data = {'data':data, 'page_range':page_range}
            return paginate_data

6.  Terdapat list yang berisi daftar teman, data daftar teman didapat menggunakan ajax.
    Membuat daftar_teman.html, membuat variabel yang parse dari methode friend_list di views.py, menampilkan beberapa kondisi yaitu ketika belum mempunyai teman dan sudah mempunyai teman. 
        var friends = JSON.parse(friend_list);
        if(friends.length == 0){
            ..
            $("#friend-list").append(html)
        }
        else{
            var counter = 0;
            $.each(friends,function(index,friend){
                ...
                var html;
                if(counter % 2 == 0){
                    ...
                }else if(counter % 2 == 1){
                    ...
                }
                $("#friend-list").append(html)
                counter += 1;
            })

7.   Buatlah tombol untuk dapat menghapus teman dari daftar teman (implementasikan menggunakan ajax).
    Pada daftar_teman.html, membuat button ketika yg akan memanggil deleteFriend dengan friend.pk yg terus bertambah jika menambahkan teman. Juga membuat function deleteFried yg merujuk ke method delete_friend di views.py.
    <button type="button" class="btn btn-xs btn-danger" onClick="deleteFriend('+friend.pk+')"></button>
    function deleteFriend(id){
        window.open('delete-friend/'+id+'/', '_self');
    }