1. Membuat Halaman Chat Box
-Tambahkan lab_6.html pada folder templates: sudah ditambahkan

-Tambahkan lab_6.css pada folder ./static/css:
membuat folder static didalamnya membuat folder css dan berisi file lab_6.css, berisi css untuk class body, chat-text, msg-send, msd-recieve.

-Tambahkan file lab_6.js pada folder lab_6/static/js
membuat file 
-Lengkapi potongan kode pada lab_6.js agar dapat berjalan
membuat fungsi:
    //chat box 
    var up = "https://png.icons8.com/collapse-arrow/win10/50/000000";
    var down = "https://png.icons8.com/expand-arrow/win10/50/000000";
    var hidden = false;
    $(".chat-text").bind("keyup", function(e) {
      console.log(e.keyCode);
      if (e.keyCode == 13 ){ //enter
        var str = $("textarea").val();
        $("textarea").val('');
        //create a new div with the message you typed and send it to front end.
        $(".msg-insert").append("<div class=\"msg-send\">" + str + "</div>");
      }
    })
  
    $("img:last-child").bind("click", function () {
      $(".chat-body").toggle("slow");
      if(!hidden) {
        hidden = true;
      } else {
        hidden = false;
        $("img").attr('src', down);
      }
    })


2. Mengimplementasikan Calculator
-Tambahkan potongan kode ke dalam file lab_6.html pada folder templates
membuat button untuk calculator

-Tambahkan potongan kode ke dalam file lab_6.css pada folder./static/css
menambahkan .calculator, .model, .calcs, #title, .btn

-Tambahkan potongan kode ke dalam file lab_6.js pada folder lab_6/static/js
-Implementasi fungsi AC.

// Calculator
var print = document.getElementById('print');
var erase = false;

var go = function(x) {
  if (x === 'ac') {
    /* implemetnasi clear all */
    print.value = '';
    erase = true;
  } else if (x === 'eval') {
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;
  else {
    print.value += x;
  }
};
function evil(fn) {
  return new Function('return ' + fn)();
}
// END

3. Mengimplementasikan select2
-Load theme default sesuai selectedTheme
    //Retrieve applied theme when browser re-opened
    if (localStorage.getItem('selected') == null){
        localStorage.setItem('selected', JSON.stringify(themes[3]));
    }
    var applied = JSON.parse(localStorage.getItem("selected"));
    $("body").css({"background-color":applied.themeObj.bcgColor, "color":applied.themeObj.fontColor});    
    });

-Populate data themes dari local storage ke select2
    $('.my-select').select2({
        'data': JSON.parse(object)
    });

-Local storage berisi themes dan selectedTheme
    localStorage.setItem("themes", JSON.stringify(themes));

    // [TODO] simpan object theme tadi ke local storage selectedTheme
    var selectedTheme = {themeObj:{"bcgColor":theme.bcgColor, "fontColor" : theme.fontColor}};
    localStorage.setItem("selected", JSON.stringify(selectedTheme));
    return false; //to break loop

-Warna berubah ketika theme dipilih
    $('.apply-button').on('click', function(){  // sesuaikan class button
        // [TODO] ambil value dari elemen select .my-select
        var data = JSON.parse(object);
        var id = $(".my-select").select2("val");
        // [TODO] cocokan ID theme yang dipilih dengan daftar theme yang ada
        $.each(data, function(i, theme) {
            if(id == theme.id) {
                // [TODO] ambil object theme yang dipilih
                themeObj = theme.text;
                // [TODO] aplikasikan perubahan ke seluruh elemen HTML yang perlu diubah warnanya
                $("body").css({"background-color" : theme.bcgColor, "color" : theme.fontColor});
                // [TODO] simpan object theme tadi ke local storage selectedTheme
                var selectedTheme = {themeObj:{"bcgColor":theme.bcgColor, "fontColor" : theme.fontColor}};
                localStorage.setItem("selected", JSON.stringify(selectedTheme));
                return false; //to break loop
            }
        });
});

4. Pastikan kalian memiliki Code Coverage yang baik
-Jika kalian belum melakukan konfigurasi untuk menampilkan Code Coverage di Gitlab maka lihat langkah Show Code Coverage in Gitlab di README.md.
-Pastikan Code Coverage kalian 100%.

5. Challenge Checklist
Implementasikan fungsi sin, log, dan tan. (HTML sudah tersedia di dalam potongan kode)
html:
<button class="btn btn-lg btn-default col-xs-3" onClick="go('log');">log</button>
<button class="btn btn-lg btn-default col-xs-3" onClick="go('sin');">sin</button>
<button class="btn btn-lg btn-default col-xs-3" onClick="go('tan');">tan</button>

var go = function(x) {
    ...
  } else if (x === 'log') {
      print.value = Math.log10(print.value);
  } else if (x === 'sin') {
      print.value = Math.sin(print.value * Math.PI / 180);
      if (print.value == 1.2246467991473532e-16) {
          print.value = 0;
      }
  } else if (x === 'tan') {
      print.value = Math.tan(print.value * Math.PI / 180); //in degrees
      if(print.value == -1.2246467991473532e-16) {
          print.value = 0;
      }
      else if(print.value == 0.9999999999999999) {
          print.value = 1;
      }
  }
  ...